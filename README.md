# Software LB

is a technique used for distribute incoming traffic across multiple servers or resources.


### Architecture

Round Robin Algorithm

![IMAGE_DESCRIPTION](https://www.nginx.com/wp-content/uploads/2014/07/what-is-load-balancing-diagram-NGINX-1024x518.png)

### Benefit

- optimal performance (high workload traffic)
- Security (hidden service IP)
- Scalability

### Software Requirement

- docker
- nginx

### handler

Not depend with infra team, on-premises by devops team

# Keepalived HA

is an open-source software tool primarily used for implementing high availability solutioN, It operates by creating a virtual IP address that is shared among multiple server.
Clients connect to this virtual IP address, and Keepalived ensures that traffic is directed to an active server.

### Architecture

- Active Passive on Server Level
- Round Robin on Application Level

![IMAGE_DESCRIPTION](https://i.ibb.co/QP7QW4D/download.png)

### Benefit

- Fault Tolerance
- Security (hidden service IP)
- Health Monitoring
- Integration with Existing Tools (nginx, apche)


### Software Requirement

- keepalved
- nginx

### handler

after apply keepalived, need infra team config on VIP to ensure that the Virtual IP (VIP) is accessible to clients.

# DNS Round Robin

is a simple technique used to distribute incoming network traffic across multiple servers or resources by rotating the order of IP addresses returned in DNS responses. 


### Architecture

- Active Passive on Server Level
- Round Robin on Application Level

![IMAGE_DESCRIPTION](https://i.ibb.co/w4bG5nB/image02.png)

### Benefit

- Fault Tolerance
- Weak Security (real ip of service)
- Less Configuration


### Software Requirement

- docker
- nginx


### handler

need infra team add record of IP service1 and service2 in single domain, then devops team can config RP and LB on applciation level
